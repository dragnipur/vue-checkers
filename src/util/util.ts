export class Util {
    static getQueryParam(key: string): string {
        const queryParams = window.location.search.substring(1)
            .split('&').map((queryParam) => {
                const split = queryParam.split('=');
                return { key: split[0], value: split[1] };
            });
            
            const foundParam = queryParams.find((param) => param.key === key);
            return foundParam ? foundParam.value : null;
    }
}