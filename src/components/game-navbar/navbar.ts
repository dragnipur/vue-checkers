import { Component, Vue } from 'vue-property-decorator';

import './navbar.scss';

@Component({
  template: require('./navbar.html'),
  props: ['activeColor']
})
export class NavbarComponent extends Vue {
  seconds: number;
  time: String = '00:00:00';
  timer: number;
  timerRunning: Boolean = false;
  activeColor: string;
  menuOpened: Boolean = false;
  inGame: Boolean = false;

  mounted() {
    this.seconds = 0;
    this.startTimer();
  }

  private resign(): void {
    this.$emit('resign');
  }

  private startTimer() {
    this.timer = window.setInterval(() => {
      this.seconds++;
      this.time = this.secondsToClock(this.seconds);
    }, 1000);
    this.timerRunning = true;
  }

  private secondsToClock(seconds: number): string {
    const minuteSeconds = seconds % 60;
    const minutes = Math.floor(seconds / 60 % 60);
    const hours = Math.floor(seconds / (60 * 60) % 60);
    return `${this.prependZero(hours)}:${this.prependZero(minutes)}:${this.prependZero(minuteSeconds)}`;
  }

  private prependZero(n: number): string {
    return n < 10 ? `0${n}` : String(n);
  }

  private pauseTimer(): void {
    if (this.timer) {
      window.clearInterval(this.timer);
      this.timer = undefined;
      this.timerRunning = false;
    } else {
      this.startTimer();
    }
  }

  private toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }

  private determineMenuClass(menuOpened) {
    return menuOpened ? 'menu--open' : 'menu--closed';
  }
}
