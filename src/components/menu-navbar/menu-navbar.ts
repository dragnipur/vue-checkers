import { Component, Vue } from 'vue-property-decorator';

import './menu-navbar.scss';

@Component({
  template: require('./menu-navbar.html')
})
export class MenuNavbarComponent extends Vue {

}
