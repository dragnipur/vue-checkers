import { Component, Vue } from 'vue-property-decorator';
import * as Vuex from 'vuex';
import { Logger } from '../../util/log';
import { PieceComponent } from '../piece';
import { Option, Piece, PieceType, Coordinate, GameType } from '../../store/model';

import './board.scss';
import { MoveCalculator } from '../../movement/moveCalculator';
import { Ai } from '../../movement/ai';
import { setTimeout } from 'timers';

@Component({
    template: require('./board.html'),
    components: {
        'piece': PieceComponent
    }
})
export class BoardComponent extends Vue {
    get board(): Array<Array<Piece>> {
        return this.$store.state.board;
    }

    private pieceClicked(event: Event, piece: Piece) {
        this.$emit('piece-clicked', piece);
    }

    public movePiece(origin: Coordinate, target: Piece) {
        const moveAnimation: AnimationKeyFrame[] = this.setupAnimation(origin, target);
        const animation = this.animatePiece(moveAnimation, origin);

        animation.onfinish = () => {
            this.$store.dispatch('movePiece', { origin: origin, target: target }).then(() => {
                this.$emit('piece-moved', target);
            });
        };
    }

    private setupAnimation(origin: Coordinate, target: Piece): AnimationKeyFrame[] {
        const column = <HTMLElement>this.$el.querySelector('.board__field');
        const animation = [{ transform: 'translate(0, 0)' }];

        for (let i = 0; i < target.moves.length; i++) {
            const yMovement = (target.moves[i].y - origin.y) * column.offsetHeight;
            const xMovement = (target.moves[i].x - origin.x) * column.offsetWidth;
            animation.push({ transform: `translate(${xMovement}px, ${yMovement}px)` });
        }

        return animation;
    }

    private animatePiece(animation: AnimationKeyFrame[], origin: Coordinate) {
        const animationSettings: AnimationEffectTiming = {
            duration: animation.length * 300 - 300,
            fill: 'forwards'
        };

        const piece = this.$el.querySelector(`.board__row:nth-child(${origin.y + 1}) 
                                              .board__field:nth-child(${origin.x + 1}) 
                                              .piece`);
        return piece.animate(animation, animationSettings);
    }
}

