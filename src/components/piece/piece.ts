import { Component, Vue } from 'vue-property-decorator';
import { Logger } from '../../util/log';
import { Coordinate, Piece, PieceType } from '../../store/model';

import './piece.scss';

@Component({
    template: require('./piece.html'),
    props: ['color', 'type', 'king', 'x', 'y']
})
export class PieceComponent extends Vue implements Piece {
    color: string;
    type: PieceType;
    king: boolean;
    coordinate: Coordinate;

    isActive(type) {
        return type !== PieceType.INACTIVE;
    }

    determineClass(type): String {
        return type === PieceType.GAME ? this.color : 'option';
    }
}