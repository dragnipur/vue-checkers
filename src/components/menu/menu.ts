import { Component, Vue } from 'vue-property-decorator';
import { MenuNavbarComponent }  from '../menu-navbar';

import './menu.scss';

@Component({
    template: require('./menu.html'),
    components: {
        'menu-navbar': MenuNavbarComponent
    }
})
export class MenuComponent extends Vue {

}
