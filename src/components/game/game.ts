import { Component, Vue } from 'vue-property-decorator';
import { Logger } from '../../util/log';
import { BoardComponent } from '../board/board';
import { PieceComponent } from '../piece/index';
import './game.scss';
import { NavbarComponent } from '../game-navbar/index';
import { Util } from '../../util/util';
import { GameType, Piece, Coordinate } from '../../store/model';
import { Ai } from '../../movement/ai';
import { GameManager } from '../../gamelogic/gameManager';
import { Factory } from '../../gamelogic/Factory';
import { join } from 'path';

@Component({
    template: require('./game.html'),
    components: {
        navbar: NavbarComponent,
        board: BoardComponent,
        piece: PieceComponent
    }
})
export class GameComponent extends Vue {
    private type: GameType;
    private gameManager: GameManager;
    private gameFinished: Boolean = false;
    private winner: String = '';

    $refs: {
        board: BoardComponent
    };

    get activeColor(): string {
        return this.$store.state.activeColor;
    }

    get redPieceCount(): number {
        return this.$store.state.redPieceCount;
    }

    get blackPieceCount(): number {
        return this.$store.state.blackPieceCount;
    }

    mounted() {
        const type = Util.getQueryParam('type');

        if (type && type.toUpperCase() in GameType) {
            const gameType = GameType[type.toUpperCase()];
            this.gameManager = Factory.getGameManager(gameType, this.$store, this.$refs.board);
            this.resetBoard();
        } else {
            this.$router.push('/');
        }
    }

    private resetBoard() {
        this.$store.dispatch('setPieceCount', { color: 'red', count: 12 });
        this.$store.dispatch('setPieceCount', { color: 'black', count: 12 });
        this.$store.dispatch('resetBoard');
    }

    private pieceClicked(piece: Piece) {
        this.gameManager.pieceClicked(piece);
    }

    private pieceMoved(target: Piece) {
        const hitColor = this.activeColor === 'red' ? 'black' : 'red';
        let pieceCount = this.subtractHitsFromPieceCount(hitColor, target.hits);

        if (pieceCount === 0) {
            this.winner = this.activeColor;
            this.gameFinished = true;
        } else {
            this.gameManager.pieceMoved();
        }
    }

    private subtractHitsFromPieceCount(color: string, hits: Array<Coordinate>): number {
        let pieceCount = color === 'red' ? this.redPieceCount : this.blackPieceCount;
        if (hits && hits.length > 0) {
            pieceCount -= hits.length;
            this.$store.dispatch('setPieceCount', { color: color, count: pieceCount });
        }
        return pieceCount;
    }

    private resign(): void {
        this.winner = this.gameManager.determineResignationWinner();
        this.gameFinished = true;
    }
}
