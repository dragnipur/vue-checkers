import Vue from 'vue';
import VueRouter, { Location, Route, RouteConfig } from 'vue-router';
import { makeHot, reload } from './util/hot-reload';

const menuComponent = () => import('./components/menu').then(({ MenuComponent }) => MenuComponent);
const gameComponent = () => import('./components/game').then(({ GameComponent }) => GameComponent);

if (process.env.ENV === 'development' && module.hot) {
  const menuModuleId = './components/menu';
  const gameModuleId = './components/game';

  // first arguments for `module.hot.accept` and `require` methods have to be static strings
  // see https://github.com/webpack/webpack/issues/5668
  makeHot(menuModuleId, menuComponent,
    module.hot.accept('./components/menu', () => reload(menuModuleId, (<any>require('./components/menu')).MenuComponent)));

  makeHot(gameModuleId, gameComponent,
    module.hot.accept('./components/game', () => reload(gameModuleId, (<any>require('./components/game')).GameComponent)));
}

Vue.use(VueRouter);

export const createRoutes: () => RouteConfig[] = () => [
  {
    path: '/',
    component: menuComponent,
  },
  {
    path: '/game',
    component: gameComponent,
  }
];

export const createRouter = () => new VueRouter({ mode: 'history', routes: createRoutes() });
