import { Piece, Move, PieceType, Option, Coordinate } from '../store/model';
import { MoveCalculator } from './moveCalculator';

export class Ai {
    private moveCalculator = new MoveCalculator();

    public determineBestMove(board: Array<Array<Piece>>): AiOption {
        let availableMoves = this.getAvailableMoves(board, 'black');
        const hitMoves = availableMoves.filter((move) => move.option.hits.length > 0);

        // If we can hit, we should pick one of those moves.
        if (hitMoves.length > 0) availableMoves = hitMoves;

        availableMoves.forEach((move) => {
            const boardCopy = JSON.parse(JSON.stringify(board));
            const baseRating = this.determineMoveRating(move, board);
            move.rating = this.determineMoveRatingInDepth(move, boardCopy, baseRating, 1);
        });

        const highestRating = Math.max.apply(Math, availableMoves.map((move) => move.rating));
        const highestRatedMoves = availableMoves.filter((move) => move.rating === highestRating);
        return highestRatedMoves[Math.floor(Math.random() * highestRatedMoves.length)];
    }

    private getAvailableMoves(board: Array<Array<Piece>>, color: string): Array<AiOption> {
        let availableMoves: Array<AiOption> = [];

        board.forEach((row) => {
            row.forEach((cell) => {
                if (cell && cell.type === PieceType.GAME && cell.color === color) {
                    const options = this.moveCalculator.getOptions(cell, board)
                        .map((option) => { return { option, rating: 0 }; });
                    availableMoves = availableMoves.concat(options);
                }
            });
        });

        return availableMoves;
    }

    private determineMoveRatingInDepth(move: AiOption, board: Array<Array<Piece>>, rating: number, moveNumber: number) {
        this.applyMoveToBoard(board, move);

        const nextColor = moveNumber % 2 === 0 ? 'red' : 'black';
        const availableMoves = this.getAvailableMoves(board, nextColor);

        let bestMove: AiOption;
        let bestRating: number;
        availableMoves.forEach((move) => {
            const boardCopy = JSON.parse(JSON.stringify(board));
            const moveRating = this.determineMoveRating(move, boardCopy);

            if (nextColor === 'black') {
                if (!bestRating || moveRating > bestRating) {
                    bestMove = move;
                }
            } else {
                if (!bestRating || moveRating < bestRating) {
                    bestMove = move;
                }
            }
        });

        if (bestMove) {
            rating += this.determineMoveRating(bestMove, board);

            if (moveNumber <= 5) {
                rating = this.determineMoveRatingInDepth(bestMove, board, rating, ++moveNumber);
            }
        }

        return rating;
    }

    private determineMoveRating(move: AiOption, board: Array<Array<Piece>>): number {
        let rating = 0;
        const piece = board[move.option.origin.y][move.option.origin.x];

        if (piece.color === 'black') {
            rating += move.option.hits.length * 2;

            if (this.pieceIsKing(piece, move.option.target)) {
                rating += 1;
            }
        } else {
            rating -= move.option.hits.length * 2;

            if (this.pieceIsKing(piece, move.option.target)) {
                rating -= 1;
            }
        }

        return rating;
    }

    private applyMoveToBoard(board: Array<Array<Piece>>, move: AiOption) {
        const piece = board[move.option.origin.y][move.option.origin.x];
        const movedPiece = { ...piece, coordinate: move.option.target, king: this.pieceIsKing(piece, move.option.target) };

        // move piece
        board[move.option.target.y][move.option.target.x] = movedPiece;
        board[move.option.origin.y][move.option.origin.x] = undefined;

        // remove hit pieces
        move.option.hits.forEach((hit) => {
            board[hit.y][hit.x] = undefined;
        });
    }

    private pieceIsKing(piece: Piece, target: Coordinate): boolean {
        return piece.king || (
            piece.color === 'red' && target.y === 7 ||
            piece.color === 'black' && target.y === 0);
    }
}

export interface AiOption {
    option: Option;
    rating: number;
}