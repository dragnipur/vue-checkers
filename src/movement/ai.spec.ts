import { expect } from 'chai';
import { Ai } from './ai';
import { PieceType, Piece, Coordinate, Option } from '../store/model';
import { fail } from 'assert';

describe('MoveCalculator', () => {
    let ai: Ai;

    beforeEach(() => {
        ai = new Ai();
    });

    it('should pick a move when the game starts', () => {
        const board = setupBoard();
        

        const move = ai.determineBestMove(board);
    });

    function setupBoard() {
        const board: Array<Array<Piece>> = Array(8);
        for (let i = 0; i < board.length; i++) {
            board[i] = new Array(8);
        }

        placePieces(board);
        return board;
    }

    function placePieces(board: Array<Array<Piece>>) {
        placePiecesInRows('red', board, [0, 1, 3]);
        placePiecesInRows('black', board, [4, 6, 7]);
    }
    
    function placePiecesInRows(color: string, board: Array<Array<Piece>>, rows: Array<number>) {
        rows.forEach((row) => {
            for (let column = 0; column < board[row].length; column++) {
                if ((row % 2 === 0 && column % 2 !== 0) ||
                    (row % 2 !== 0 && column % 2 === 0)) {
    
                    const coordinate = { y: row, x: column };
                    board[row].splice(column, 1, { type: PieceType.GAME, color: color, king: false, coordinate });
                }
            }
        });
    }
});
