import { Option, Coordinate, Piece, PieceType } from '../store/model';

export class MoveCalculator {
    public getOptions(piece: Piece, board: Array<Array<Piece>>): Array<Option> {
        const boardCopy = JSON.parse(JSON.stringify(board));

        if (piece.king) {
            return this.determineKingOptions(piece, boardCopy);
        } else {
            return this.determineRegularPieceOptions(piece, boardCopy);
        }
    }

    private determineRegularPieceOptions(piece: Piece, board: Array<Array<Piece>>): Array<Option> {
        let options: Array<Option> = [];
        const rowMultiplier = piece.color === 'red' ? 1 : -1;

        // regular move
        [piece.coordinate.x - 1, piece.coordinate.x + 1].forEach((column) => {
            if (this.isEmptyColumn(board, piece.coordinate.y + rowMultiplier, column)) {
                const target = { y: piece.coordinate.y + rowMultiplier, x: column };
                options.push({origin: piece.coordinate, target: target, hits: [], moves: [target] });
            }
        });

        return options.concat(this.getHitOptions(piece, board));
    }

    private getHitOptions(piece: Piece, board: Array<Array<Piece>>, previousOption?: Option): Array<Option> {
        let options: Array<Option> = [];

        const enemyRow = piece.color === 'red' ? piece.coordinate.y + 1 : piece.coordinate.y - 1;
        const targetRow = piece.color === 'red' ? piece.coordinate.y + 2 : piece.coordinate.y - 2;

        // check if a hit is possible (left [x - 1] and right [x + 1 ])
        [1, -1].forEach((xMultiplier) => {
            if (this.columnContainsEnemy(board, piece, enemyRow, piece.coordinate.x + 1 * xMultiplier) &&
                this.isEmptyColumn(board, targetRow, piece.coordinate.x + 2 * xMultiplier)) {

                // hit possible, add an option with the hit information
                const hit = { y: enemyRow, x: piece.coordinate.x + 1 * xMultiplier };
                const hits = previousOption ? previousOption.hits.concat(hit) : [hit];
                const move = { y: targetRow, x: piece.coordinate.x + 2 * xMultiplier };
                const moves = previousOption ? previousOption.moves.concat(move) : [move];
                const origin = previousOption ? previousOption.origin : piece.coordinate;
                const option = { origin, target: { y: targetRow, x: piece.coordinate.x + 2 * xMultiplier }, hits, moves };
                options.push(option);

                // see if there is another hit possible from the option position
                options = options.concat(
                    this.getHitOptions(
                        { type: PieceType.GAME, color: piece.color, coordinate: { y: targetRow, x: piece.coordinate.x + 2 * xMultiplier } },
                        board,
                        option)
                );
            }
        });
        
        return options;
    }

    private determineKingOptions(piece: Piece, board: Array<Array<Piece>>): Array<Option> {
        let options: Array<Option> = [];

        // regular move
        [1, -1].forEach((movement) => {
            if (this.isEmptyColumn(board, piece.coordinate.y + movement, piece.coordinate.x + movement)) {
                const target = { y: piece.coordinate.y + movement, x: piece.coordinate.x + movement };
                options.push({ origin: piece.coordinate, target: target, hits: [], moves: [target] });
            }

            if (this.isEmptyColumn(board, piece.coordinate.y - movement, piece.coordinate.x + movement)) {
                const target = { y: piece.coordinate.y - movement, x: piece.coordinate.x + movement };
                options.push({ origin: piece.coordinate, target: target, hits: [], moves: [target] });
            }
        });

        return options.concat(this.getKingHitOptions(piece, board));
    }

    private getKingHitOptions(piece: Piece, board: Array<Array<Piece>>, previousOption?: Option): Array<Option> {
        let options: Array<Option> = [];

        // check if a hit is possible
        [1, -1].forEach((yMultiplier) => {
            [1, -1].forEach((xMultiplier) => {
                const enemyCoordinate: Coordinate = { x: piece.coordinate.x + 1 * xMultiplier, y: piece.coordinate.y + 1 * yMultiplier };
                const targetCoordinate: Coordinate = { x: piece.coordinate.x + 2 * xMultiplier, y: piece.coordinate.y + 2 * yMultiplier };

                if (this.columnContainsEnemy(board, piece, enemyCoordinate.y, enemyCoordinate.x) &&
                    this.isEmptyColumn(board, targetCoordinate.y, targetCoordinate.x) && 
                    this.pieceIsNotHitByPreviousOption(enemyCoordinate, previousOption)) {

                    // hit possible, add an option with the hit information
                    const hit = { y: enemyCoordinate.y, x: enemyCoordinate.x };
                    const hits = previousOption ? previousOption.hits.concat(hit) : [hit];
                    const move = { y: targetCoordinate.y, x: targetCoordinate.x};
                    const moves = previousOption ? previousOption.moves.concat(move) : [move];
                    const origin = previousOption ? previousOption.origin : piece.coordinate;
                    const option = { origin, target: { y: targetCoordinate.y, x: targetCoordinate.x }, hits, moves };
                    options.push(option);

                    // see if there is another hit possible from the option position
                    options = options.concat(
                        this.getKingHitOptions(
                            { type: PieceType.GAME, color: piece.color, coordinate: { y: targetCoordinate.y, x: targetCoordinate.x } },
                            board,
                            option)
                    );
                }
            });
        });

        return options;
    }

    private isEmptyColumn(board: Array<Array<Piece>>, row: number, column: number) {
        return this.isValidColumn(board, row, column) && !board[row][column];
    }

    private columnContainsEnemy(board: Array<Array<Piece>>, piece, row: number, column: number) {
        return this.isValidColumn(board, row, column) &&
            !this.isEmptyColumn(board, row, column) &&
            board[row][column].color !== piece.color;
    }

    private isValidColumn(board: Array<Array<Piece>>, row: number, column: number): Boolean {
        return column >= 0 && row >= 0 && row < board.length && column < board[row].length;
    }

    private pieceIsNotHitByPreviousOption(pieceCoordinate: Coordinate, previousOption: Option): Boolean {
        if (previousOption) {
            const hits = previousOption.hits;
            for (let i = 0; i < hits.length; i++) {
                if (pieceCoordinate.x === hits[i].x && pieceCoordinate.y === hits[i].y) return false;
            }
        }
        return true;
    }
}