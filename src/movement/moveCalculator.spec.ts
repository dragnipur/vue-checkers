import { expect } from 'chai';
import { MoveCalculator } from './moveCalculator';
import { PieceType, Piece, Coordinate, Option } from '../store/model';
import { fail } from 'assert';

describe('MoveCalculator', () => {
    let moveCalculator: MoveCalculator;

    beforeEach(() => {
        moveCalculator = new MoveCalculator();
    });

    it('should return 2 options when a regular piece with both move cells free is given.', () => {
        const board = setupEmptyBoard();
        const piece = { type: PieceType.GAME, coordinate: { y: 2, x: 1 }, color: 'red' };
        board[2][1] = piece;

        const options = moveCalculator.getOptions(piece, board);

        expect(options.length).to.equal(2);
        validateOption(options[0], piece.coordinate, { y: 3, x: 0 });
        validateOption(options[1], piece.coordinate, { y: 3, x: 2 });
    });

    it('should return 1 option when a regular piece with 1 move cell free and a red piece on the other cell is given.', () => {
        const board = setupEmptyBoard();
        const piece1 = { type: PieceType.GAME, coordinate: { y: 2, x: 1 }, color: 'red' };
        board[2][1] = piece1;

        const piece2 = { type: PieceType.GAME, coordinate: { y: 3, x: 0 }, color: 'red' };
        board[3][0] = piece2;

        const options = moveCalculator.getOptions(piece1, board);

        expect(options.length).to.equal(1);
    });

    it('should return 0 options when both move cells have a red piece on it.', () => {
        const board = setupEmptyBoard();
        const piece1 = { type: PieceType.GAME, coordinate: { y: 2, x: 1 }, color: 'red' };
        board[2][1] = piece1;

        const piece2 = { type: PieceType.GAME, coordinate: { y: 3, x: 0 }, color: 'red' };
        board[3][0] = piece2;

        const piece3 = { type: PieceType.GAME, coordinate: { y: 3, x: 2 }, color: 'red' };
        board[3][2] = piece3;

        const options = moveCalculator.getOptions(piece1, board);

        expect(options.length).to.equal(0);
    });

    it('should return 0 options when a red piece is on one move cell and black pieces are on the second and cell underneath it.', () => {
        const board = setupEmptyBoard();
        const piece1 = { type: PieceType.GAME, coordinate: { y: 2, x: 1 }, color: 'red' };
        board[2][1] = piece1;

        const piece2 = { type: PieceType.GAME, coordinate: { y: 3, x: 0 }, color: 'red' };
        board[3][0] = piece2;

        const piece3 = { type: PieceType.GAME, coordinate: { y: 3, x: 2 }, color: 'black' };
        board[3][2] = piece3;

        const piece4 = { type: PieceType.GAME, coordinate: { y: 3, x: 2 }, color: 'black' };
        board[4][3] = piece4;

        const options = moveCalculator.getOptions(piece1, board);

        expect(options.length).to.equal(0);
    });

    it(`should return 1 option with 1 hit when a red piece is on the first move cell and a 
        black piece is on a move cell and the cell behind it is empty.`, () => {
            const board = setupEmptyBoard();
            const piece1 = { type: PieceType.GAME, coordinate: { y: 2, x: 1 }, color: 'red' };
            board[2][1] = piece1;

            const piece2 = { type: PieceType.GAME, coordinate: { y: 3, x: 0 }, color: 'red' };
            board[3][0] = piece2;

            const piece3 = { type: PieceType.GAME, coordinate: { y: 3, x: 2 }, color: 'black' };
            board[3][2] = piece3;

            const options = moveCalculator.getOptions(piece1, board);

            expect(options.length).to.equal(1);
            validateOption(options[0], piece1.coordinate, { y: 4, x: 3 });

            expect(options[0].hits.length).to.equal(1);
            expect(options[0].hits[0].x).to.equal(2);
            expect(options[0].hits[0].y).to.equal(3);

            expect(options[0].moves.length).to.equal(1);
            expect(options[0].moves[0].x).to.equal(3);
            expect(options[0].moves[0].y).to.equal(4);
        });

    it('should return 1 option with 2 hits when 2 black pieces are underneath in a row with empty cells between them.', () => {
        const board = setupEmptyBoard();
        const piece1 = { type: PieceType.GAME, coordinate: { y: 2, x: 1 }, color: 'red' };
        board[2][1] = piece1;

        const piece2 = { type: PieceType.GAME, coordinate: { y: 3, x: 0 }, color: 'red' };
        board[3][0] = piece2;

        const piece3 = { type: PieceType.GAME, coordinate: { y: 3, x: 2 }, color: 'black' };
        board[3][2] = piece3;

        const piece4 = { type: PieceType.GAME, coordinate: { y: 5, x: 4 }, color: 'black' };
        board[5][4] = piece4;

        const options = moveCalculator.getOptions(piece1, board);

        expect(options.length).to.equal(2);
        validateOption(options[0], piece1.coordinate, { y: 4, x: 3 });
        expect(options[0].hits.length).to.equal(1);
        expect(options[0].hits[0].x).to.equal(2);
        expect(options[0].hits[0].y).to.equal(3);

        validateOption(options[1], piece1.coordinate, { y: 6, x: 5 });
        expect(options[1].hits.length).to.equal(2);
        expect(options[1].hits[0].y).to.equal(3);
        expect(options[1].hits[0].x).to.equal(2);
        expect(options[1].hits[1].y).to.equal(5);
        expect(options[1].hits[1].x).to.equal(4);
    });

    function setupEmptyBoard() {
        const board: Array<Array<Piece>> = Array(8);
        for (let i = 0; i < board.length; i++) {
            board[i] = new Array(8);
        }
        return board;
    }

    function validateOption(option: Option, expectedOrigin: Coordinate, expectedTarget: Coordinate) {
        expect(option.origin).to.equal(expectedOrigin);
        expect(option.target.x).to.equal(expectedTarget.x);
        expect(option.target.y).to.equal(expectedTarget.y);
    }
});
