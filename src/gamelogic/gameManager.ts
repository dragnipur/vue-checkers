import { Piece, Option } from '../store/model';
import { Store } from 'vuex';
import { MoveCalculator } from '../movement/moveCalculator';
import { BoardComponent } from '../components/board/index';

export abstract class GameManager {
    protected store: Store<any>;
    protected board: BoardComponent;
    protected moveCalculator: MoveCalculator;
    protected options: Array<Option>;
    protected selectedPiece: Piece;

    constructor(store: Store<any>, board: BoardComponent) {
        this.store = store;
        this.board = board;
        this.moveCalculator = new MoveCalculator();
    }
    
    abstract pieceClicked(piece: Piece): void;
    abstract pieceMoved(): void;
    abstract determineResignationWinner(): string;

    protected clearOptions() {
        if (this.options) {
            this.store.dispatch('clearOptions', this.options);
        }
        this.options = new Array();
    }

    protected addOptionsToBoard() {
        this.store.dispatch('addOptionsToBoard', this.options);
    }
}