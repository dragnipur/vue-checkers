import { GameManager }  from './gameManager';
import { Piece, PieceType } from '../store/model';
import { types } from '../store/mutation-types';

export class MultiplayerManager extends GameManager {

    pieceClicked(piece: Piece): void {
        this.clearOptions();

        if (piece.type === PieceType.GAME && piece.color === this.store.state.activeColor) {
            this.selectedPiece = piece;
            this.options = this.moveCalculator.getOptions(piece, this.store.state.board);
            this.addOptionsToBoard();
        } else if (piece.type === PieceType.OPTION) {
            const origin = { x: this.selectedPiece.coordinate.x, y: this.selectedPiece.coordinate.y };
            this.board.movePiece(origin, piece);
        }
    }

    pieceMoved(): void {
        const newActiveColor = this.store.state.activeColor === 'red' ? 'black' : 'red';
        this.store.commit(types.activeColor, newActiveColor);
    }

    determineResignationWinner(): string {
        return this.store.state.activeColor === 'red' ? 'black' : 'red';
    }
}