import { GameManager } from './gameManager';
import { Piece, PieceType } from '../store/model';
import { types } from '../store/mutation-types';
import { Ai } from '../movement/ai';

export class SingleplayerManager extends GameManager {
    private ai: Ai = new Ai();

    pieceClicked(piece: Piece): void {
        if (this.store.state.activeColor === 'red') {
            this.clearOptions();
            if (this.isPieceWhichCanBeMoved(piece)) {
                this.selectedPiece = piece;
                this.options = this.moveCalculator.getOptions(piece, this.store.state.board);
                this.addOptionsToBoard();
            } else if (piece.type === PieceType.OPTION) {
                const origin = { x: this.selectedPiece.coordinate.x, y: this.selectedPiece.coordinate.y };
                this.board.movePiece(origin, piece);
            }
        }
    }

    private isPieceWhichCanBeMoved(piece: Piece) {
        return piece.type === PieceType.GAME &&
            this.store.state.activeColor === 'red' &&
            piece.color === 'red';
    }

    pieceMoved(): void {
        const newActiveColor = this.store.state.activeColor === 'red' ? 'black' : 'red';
        this.store.commit(types.activeColor, newActiveColor);
        if (newActiveColor === 'black') this.makeAiMove();
    }

    private makeAiMove() {
        const move = this.ai.determineBestMove(this.store.state.board);
        const origin = move.option.origin;
        const target = move.option.target;
        const piece = this.store.state.board[origin.y][origin.x];

        this.options = this.moveCalculator.getOptions(piece, this.store.state.board);

        setTimeout(() => {
            this.addOptionsToBoard();

            setTimeout(() => {
                const option = this.store.state.board[target.y][target.x];
                this.board.movePiece(origin, option);
                this.clearOptions();
            }, 200);
        }, 200);
    }

    determineResignationWinner(): string {
        return 'black';
    }
}