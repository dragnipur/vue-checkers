import { GameType } from '../store/model';
import { MultiplayerManager } from './multiplayerManager';
import { SingleplayerManager } from './singleplayerManager';
import { OnlineManager } from './onlineManager';
import { Store } from 'vuex';
import { BoardComponent } from '../components/board/index';
import { GameManager } from './gameManager';

export class Factory {
    public static getGameManager(type: GameType, store: Store<any>, board: BoardComponent): GameManager {
        switch (type) {
            case GameType.MULTIPLAYER:
                return new MultiplayerManager(store, board);
            case GameType.SINGLEPLAYER:
                return new SingleplayerManager(store, board);
            case GameType.ONLINE:
                return new OnlineManager(store, board);
            default: 
                throw new Error(`Invalid game type: ${type}`);
        }
    }
}