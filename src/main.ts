import Vue from 'vue';
import VueRouter from 'vue-router';
import { makeHot, reload } from './util/hot-reload';
import { createRouter } from './router';
import store from './store';

import './sass/main.scss';

new Vue({
  el: '#app-main',
  store,
  router: createRouter(),
  components: {
  }
});
