import { StoreOptions } from 'vuex';
import { State, Piece, Coordinate } from './model';

export class BaseState implements State {
    public board: Array<Array<Piece>> = [];
    public activeColor: string = 'red';
    public redPieceCount: number = 12;
    public blackPieceCount: number = 12;
}
