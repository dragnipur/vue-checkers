import { ActionContext, ActionTree } from 'vuex';
import { types } from './mutation-types';
import { BaseState } from './state';
import { Option, Coordinate, PieceType, Piece, Move, SetPieceCountAction } from './model';

export const actions: ActionTree<BaseState, BaseState> = {
    resetBoard({ commit }: ActionContext<BaseState, BaseState>): void {
        const board = new Array(8);
        for (let i = 0; i < board.length; i++) {
            board[i] = new Array(8);
        }

        placePieces(board);
        commit(types.board, board);
        commit(types.activeColor, 'red');
    },

    clearOptions({ commit, state }: ActionContext<BaseState, BaseState>, options: Array<Option>): void {
        const board = JSON.parse(JSON.stringify(state.board));

        options.forEach((option) => {
            if (board[option.target.y][option.target.x].type === PieceType.OPTION) {
                board[option.target.y][option.target.x] = undefined;
            }
        });

        commit(types.board, board);
    },

    addOptionsToBoard({ commit, state }: ActionContext<BaseState, BaseState>, options: Array<Option>): void {
        const board = JSON.parse(JSON.stringify(state.board));

        options.forEach((option) => {
            board[option.target.y][option.target.x] = { 
                type: PieceType.OPTION, 
                coordinate: option.target, 
                hits: option.hits, 
                moves: option.moves 
            };
        });

        commit(types.board, board);
    },

    movePiece({ commit, state }: ActionContext<BaseState, BaseState>, action: Move): Promise<void> {
        return new Promise((resolve, reject) => {
            const board = JSON.parse(JSON.stringify(state.board));
            const piece = board[action.origin.y][action.origin.x];
            const movedPiece = { ...piece, coordinate: action.target.coordinate, king: pieceIsKing(piece, action.target.coordinate) };
    
            // move piece
            board[action.target.coordinate.y][action.target.coordinate.x] = movedPiece;
            board[action.origin.y][action.origin.x] = undefined;
    
            // remove hit pieces
            action.target.hits.forEach((hit) => {
                board[hit.y][hit.x] = undefined;
            });
    
            commit(types.board, board);
            resolve();
        });
    },

    setPieceCount({commit, state }: ActionContext<BaseState, BaseState>, action: SetPieceCountAction) {
        if (action.color === 'red') {
            commit(types.redPieceCount, action.count);
        } else if (action.color === 'black') {
            commit(types.blackPieceCount, action.count);
        }
    }
};

function placePieces(board: Array<Array<Piece>>) {
    placePiecesInRows('red', board, [0, 1, 2]);
    placePiecesInRows('black', board, [5, 6, 7]);
}

function placePiecesInRows(color: string, board: Array<Array<Piece>>, rows: Array<number>) {
    rows.forEach((row) => {
        for (let column = 0; column < board[row].length; column++) {
            if ((row % 2 === 0 && column % 2 !== 0) ||
                (row % 2 !== 0 && column % 2 === 0)) {

                const coordinate = { y: row, x: column };
                board[row].splice(column, 1, { type: PieceType.GAME, color: color, king: false, coordinate });
            }
        }
    });
}

function pieceIsKing(piece: Piece, target: Coordinate): Boolean {
    return piece.king || (
        piece.color === 'red' && target.y === 7 ||
        piece.color === 'black' && target.y === 0);
}