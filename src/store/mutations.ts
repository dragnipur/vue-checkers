import { MutationTree } from 'vuex';

import { State, Piece, Coordinate } from './model';
import { BaseState } from './state';
import { types } from './mutation-types';

export const mutations: MutationTree<State> = {
    [types.board](state: State, board: Array<Array<Piece>>) {
        state.board = board;
    },
    [types.activeColor](state: State, activeColor: string) {
        state.activeColor = activeColor;
    },
    [types.redPieceCount](state: State, count: number) {
        state.redPieceCount = count;
    },
    [types.blackPieceCount](state: State, count: number) {
        state.blackPieceCount = count;
    }
};
