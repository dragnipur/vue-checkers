import Vue from 'vue';
import Vuex, { Store } from 'vuex';

import { BaseState } from './state';

import { actions } from './actions';
import { mutations } from './mutations';

const debug: boolean = process.env.NODE_ENV !== 'production';

Vue.use(Vuex);

export default new Store<BaseState>({
 state: new BaseState(),
mutations,
actions,
strict: debug
});