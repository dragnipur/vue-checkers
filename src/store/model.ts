export interface Piece {
    type: PieceType;
    color?: string;
    king?: boolean;
    coordinate: Coordinate;
    hits?: Array<Coordinate>;
    moves?: Array<Coordinate>;
}
export enum PieceType {
    GAME, OPTION, INACTIVE
}

export enum GameType {
    SINGLEPLAYER = 'SINGLEPLAYER', 
    MULTIPLAYER = 'MULTIPLAYER',
    ONLINE = 'ONLINE'
}

export interface Coordinate {
    x: number;
    y: number;
}

export interface State {
    board: Array<Array<Piece>>;
    activeColor: string;
    redPieceCount: number;
    blackPieceCount: number;
}

export interface Option {
    origin: Coordinate;
    target: Coordinate;
    hits: Array<Coordinate>;
    moves: Array<Coordinate>;
}

export interface Move {
    origin: Coordinate;
    target: Piece;
}

export interface SetPieceCountAction {
    color: string;
    count: number;
}