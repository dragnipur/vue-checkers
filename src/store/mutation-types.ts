export const types = {
    board: 'BOARD',
    activeColor: 'ACTIVE_COLOR',
    redPieceCount: 'RED_PIECE_COUNT',
    blackPieceCount: 'BLACK_PIECE_COUNT'
};
